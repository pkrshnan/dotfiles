call plug#begin()

Plug 'scrooloose/nerdtree'
Plug 'mattn/emmet-vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } 
Plug 'junegunn/fzf.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'lervag/vimtex'
Plug 'junegunn/goyo.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'ryanoasis/vim-devicons'
Plug 'sheerun/vim-polyglot'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-jedi'
Plug 'tweekmonster/django-plus.vim'
Plug 'wlangstroth/vim-racket'
Plug 'dylanaraps/wal.vim'
Plug 'itchyny/lightline.vim'


call plug#end()
filetype plugin indent on

" Theming
syntax on
set noshowmode

colorscheme wal

let g:lightline = { 'colorscheme': 'wal'}

let g:lightline.mode_map = {
    \   'n'      : ' N ',
    \   'i'      : ' I ',
    \   'R'      : ' R ',
    \   'v'      : ' V ',
    \   'V'      : 'V-L',
    \   'c'      : ' C ',
    \   "\<C-v>" : 'V-B',
    \   's'      : ' S ',
    \   'S'      : 'S-L',
    \   "\<C-s>" : 'S-B',
    \   "t"      : ' T ',
    \   '?'      : ' ? '
    \ }

" Ease of Use
set number
set tabstop=2 
set shiftwidth=2 
set expandtab
set hlsearch


"Functionality
au BufRead,BufNewFile *.vue set ft=html
let g:deoplete#enable_at_startup = 1 
" Mapping
map <F2> :set paste<CR>i   
map <F3> :set nopaste<CR>
map <C-p> :Files<CR>

imap <expr> <C-y> emmet#expandAbbrIntelligent("\<tab>")

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

map <F10> :NERDTreeToggle<CR>
map <F9> :NERDTreeFind<CR>
