export PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="spaceship"
plugins=(git tig docker docker-compose golang pip )
plugins+=( history history-substring-search httpie sudo vagrant postgres )
plugins+=( osx lein node npm jump gulp mosh )
plugins+=( k z alias-tips zsh-completions almostontop zsh-autosuggestions )
plugins+=( zsh-syntax-highlighting )

source $ZSH/oh-my-zsh.sh

##### EXPORT

# Nopode.JS
#export PATH="$PATH:`yarn global bin`"

## JAVA / CLOJURE
export M2=$HOME/.m2
export PATH=$PATH:$M2

## GO
export GOPATH=$HOME/code/golang
export PATH=$GOPATH/bin:/usr/local/go/bin:$PATH

# Make zsh know about hosts already accessed by SSH
zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'


## CUSTOM FUNCTIONS

# Create a new directory and enter it
function mkd() {
	mkdir -p "$@" && cd "$_";
}
function mdt() {
    markdown "$*" | lynx -stdin
}

function mdb() {
    local TMPFILE=$(mktemp)
    markdown "$*" > $TMPFILE && ( xdg-open $TMPFILE > /dev/null 2>&1 & )
}
# Git clone + npm install
function gcn {
    url=$1;
    if [ -n "${1}" ]; then
        echo 'OK'
    else
        echo 'Koooooooooooooooo'
    fi
    cd ~/code;
    reponame=$(echo $url | awk -F/ '{print $NF}' | sed -e 's/.git$//');
    git clone $url $reponame;
    cd $reponame;
    npm install;
}

export PATH=/home/pranavk/code/golang/bin:/usr/local/go/bin:/home/pranavk/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/pranavk/.m2:/home/pranavk/.vimpkg/bin

alias vim=nvim
alias vi=nvim
alias ls=colorls
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source /opt/ros/melodic/setup.zsh
